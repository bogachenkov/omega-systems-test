# Installation

### Install the dependencies

```sh
$ npm install
```

### Run application

```sh
$ npm start
```
