import { Route, Redirect, withRouter } from 'react-router-dom';
import React, { Component } from 'react';

class PrivateRoute extends Component {
  render() {
    const { auth } = this.props;
    return (
      auth ?
      <Route {...this.props} /> :
      <Redirect to="/signin" />
    );
  }
}

export default withRouter(PrivateRoute);
