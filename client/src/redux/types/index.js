const createAsyncActionType = type => ({
  PENDING: `${type}_PENDING`,
  SUCCESS: `${type}_SUCCESS`,
  FAILS: `${type}_FAILS`,
});

export const SIGN_IN = createAsyncActionType('SIGN_IN');
export const SIGN_UP = createAsyncActionType('SIGN_UP');
export const SIGN_OUT = "SIGN_OUT";

export const CHECK_TOKEN = createAsyncActionType('CHECK_TOKEN');

export const FETCH_MARKUPS = createAsyncActionType('FETCH_MARKUPS');
export const SAVE_MARKUP = createAsyncActionType('SAVE_MARKUP');

export const UI_PENDING_HANDLER = "UI_PENDING_HANDLER";
export const UI_SUCCESS_HANDLER = "UI_SUCCESS_HANDLER";
export const UI_FAILS_HANDLER = "UI_FAILS_HANDLER";
export const REMOVE_ERROR = "REMOVE_ERROR";

export const FETCH_USERS = createAsyncActionType('FETCH_USERS');
export const ADD_USER = createAsyncActionType('ADD_USER');
export const BLOCK_USER = createAsyncActionType('BLOCK_USER');

export const FETCH_DATASETS = createAsyncActionType('FETCH_DATASETS');
export const ADD_DATASET = createAsyncActionType('ADD_DATASET');
export const EDIT_DATASET = createAsyncActionType('EDIT_DATASET');
