import axios from 'axios';

import { SIGN_UP } from '../types';

// Sign Up action
export const signUp = credentials => dispatch => {
  dispatch({type: SIGN_UP.PENDING});
  axios.post('/auth/register', credentials)
    .then(response => {
      const {token, user} = response.data;
      
      //Save token to localStorage and set it to headers
      localStorage.setItem('token', token);
      axios.defaults.headers.common['Authorization'] = token;

      if (token) dispatch({
        type: SIGN_UP.SUCCESS,
        payload: {
          user
        }
      })
    })
    .catch(error => dispatch({
      type: SIGN_UP.FAILS,
      payload: {
        error: error.response.data
      }
    }))
};
