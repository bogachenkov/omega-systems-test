import axios from 'axios';
import history from '../../history';
import { FETCH_DATASETS, ADD_DATASET } from '../types';

export const fetchDatasets = () => dispatch => {
  dispatch({type: FETCH_DATASETS.PENDING});
  axios.get('/datasets')
    .then(response => dispatch({
      type: FETCH_DATASETS.SUCCESS,
      payload: {
        datasets: response.data
      }
    }))
    .catch(error => dispatch({
      type: FETCH_DATASETS.FAILS,
      payload: {
        error
      }
    }))
}

export const addDataset = (credentials) => dispatch => {
  dispatch({ type: ADD_DATASET.PENDING });
  axios.post('/datasets/add', credentials)
    .then(response => {
      history.push("/dashboard/datasets");
      return dispatch({
        type: ADD_DATASET.SUCCESS,
        payload: {
          dataset: response.data
        }
      })
    })
    .catch(error => dispatch({
      type: ADD_DATASET.FAILS,
      payload: {
        error: error.response.data
      }
    }))
}
