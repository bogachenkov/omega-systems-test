import axios from 'axios';
import { CHECK_TOKEN, SIGN_OUT } from '../types';

// Check token when user enters the site
export const checkToken = () => dispatch => {
  const token = localStorage.getItem('token');
  // If no token found, user should sign in
  if (!token) {
    dispatch({
      type: SIGN_OUT
    })
  } else {
    // Else get user data by token
    axios.defaults.headers.common['Authorization'] = token;

    dispatch({type: CHECK_TOKEN.PENDING});

    axios.get(`/auth/user`)
      .then(response => {
        dispatch({
          type: CHECK_TOKEN.SUCCESS,
          payload: {
            user: response.data
          }
        })
      })
      .catch(error => {
        // If token invalid or expired remove it and logout user
        if (error.response && error.response.status === 401) {
          localStorage.removeItem('token');
          dispatch({ type: CHECK_TOKEN.FAILS });
          dispatch({type: SIGN_OUT});
        }
      })
  }
}
