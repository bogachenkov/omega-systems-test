import axios from 'axios';
import { FETCH_MARKUPS, SAVE_MARKUP } from '../types';

// Get unmarked images from server
export const fetchMarkups = (folder) => dispatch => {
  dispatch({ type: FETCH_MARKUPS.PENDING });
  axios.get(`/markups/${folder}`)
    .then(response => {
      dispatch({
        type: FETCH_MARKUPS.SUCCESS,
        payload: {
          markups: response.data
        }
      })
    })
    .catch(error => {
      dispatch({
        type: FETCH_MARKUPS.FAILS,
        payload: {
          error: error.response.data
        }
      })
    })
}

// Save marked image to database
export const saveMarkup = data => dispatch => {
  dispatch({ type: SAVE_MARKUP.PENDING });
  axios.post('/markups/save', data)
    .then(response => {
      console.log(response.data.name);
      dispatch({
        type: SAVE_MARKUP.SUCCESS,
        payload: {
          savedMarkupName: response.data.name
        }
      })
    })
    .catch(error => {
      dispatch({
        type: SAVE_MARKUP.FAILS,
        payload: {
          error: error.response.data
        }
      })
    })
}
