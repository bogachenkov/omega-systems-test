import axios from 'axios';
import history from '../../history';
import { FETCH_USERS, ADD_USER, BLOCK_USER } from '../types';

export const fetchUsers = () => dispatch => {
  dispatch({type: FETCH_USERS.PENDING});
  axios.get('/users')
    .then(response => dispatch({
      type: FETCH_USERS.SUCCESS,
      payload: {
        users: response.data
      }
    }))
    .catch(error => dispatch({
      type: FETCH_USERS.FAILS,
      payload: {
        error: error.response.data
      }
    }))
}

export const addUser = (credentials) => dispatch => {
  dispatch({ type: ADD_USER.PENDING });
  axios.post('/users/add', credentials)
    .then(response => {
      history.push("/dashboard/users");
      dispatch({
        type: ADD_USER.SUCCESS,
        payload: {
          user: response.data
        }
      })
    })
    .catch(error => dispatch({
      type: ADD_USER.FAILS,
      payload: {
        error: error.response.data
      }
    }))
}

export const toggleBlockUser = (user_id) => dispatch => {
  dispatch({ type: BLOCK_USER.PENDING });
  axios.put(`/users/${user_id}/block`)
    .then(response => dispatch({
      type: BLOCK_USER.SUCCESS,
      payload: {
        user: response.data
      }
    }))
    .catch(error => dispatch({
      type: BLOCK_USER.FAILS,
      payload: {
        error: error.response.data
      }
    }))
}
