import axios from 'axios';
import { SIGN_IN, SIGN_OUT } from '../types';

// Sign In action
export const signIn = credentials => dispatch => {
  dispatch({type: SIGN_IN.PENDING});
  axios.post('/auth/login', credentials)
    .then(response => {
      const {token, user} = response.data;

      //Save token to localStorage and set it to headers
      localStorage.setItem('token', token);
      axios.defaults.headers.common['Authorization'] = token;

      dispatch({
        type: SIGN_IN.SUCCESS,
        payload: {
          user
        }
      })
    })
    .catch(error => dispatch({
      type: SIGN_IN.FAILS,
      payload: {
        error: error.response.data
      }
    }))
};

// Sign Out action
export const signOut = () => dispatch => {
  localStorage.removeItem('token');
  dispatch({type: SIGN_OUT});
}
