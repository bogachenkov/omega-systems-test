import { UI_PENDING_HANDLER, UI_SUCCESS_HANDLER, UI_FAILS_HANDLER } from '../types';

// Middleware catches pending/success/fails actions and dispatch UI actions
const uiMiddleware = ({dispatch}) => next => action => {
  if (typeof action === 'function') return next(action);
  if (action.type.startsWith('CHECK_TOKEN')) return next(action);
  if (action.type.endsWith('_PENDING')) {
    dispatch({type: UI_PENDING_HANDLER});
  }
  if (action.type.endsWith('_SUCCESS')) {
    dispatch({type: UI_SUCCESS_HANDLER});
  }
  if (action.type.endsWith('_FAILS')) {
    dispatch({
      type: UI_FAILS_HANDLER,
      payload: {
        error: action.payload ? action.payload.error : ''
      }
    })
  }
  return next(action);
}

export default uiMiddleware;
