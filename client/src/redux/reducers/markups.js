import { FETCH_MARKUPS, SAVE_MARKUP, SIGN_OUT } from '../types';

const initialState = {
  arr: []
}

const removeMarkedImage = (state, name) => {
  console.log(name);
  const images = [...state.arr];
  const index = images.indexOf(name);
  console.log(index);
  if (index !== -1) images.splice(index, 1);
  return images;
}

const shuffle = arr => {
    const shuffleArray = [...arr];
    for (let i = shuffleArray.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [shuffleArray[i], shuffleArray[j]] = [shuffleArray[j], shuffleArray[i]];
    }
    return shuffleArray;
}

export default function(state = initialState, {type, payload}) {
  switch (type) {
    case FETCH_MARKUPS.SUCCESS:
      return {
        ...state,
        arr: shuffle(payload.markups)
      }
    case SAVE_MARKUP.SUCCESS:
      return {
        ...state,
        arr: shuffle(removeMarkedImage(state, payload.savedMarkupName))
      }
    case SIGN_OUT:
      return {
        ...state,
        arr: []
      }
    default:
      return state;
  }
}
