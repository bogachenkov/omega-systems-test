import { SIGN_IN, SIGN_UP, SIGN_OUT, CHECK_TOKEN } from '../types';

const initialState = {
  isAuth: false,
}

export default function(state = initialState, {type, payload}) {
  switch (type) {
    case SIGN_IN.SUCCESS:
      return {
        ...state,
        isAuth: true,
      }
    case SIGN_IN.FAILS:
      return {
        ...state,
        isAuth: false,
      }
    case CHECK_TOKEN.SUCCESS:
      return {
        ...state,
        isAuth: true,
      }
    case SIGN_UP.SUCCESS:
      return {
        ...state,
        isAuth: true,
      }
    case SIGN_OUT:
      return {
        ...state,
        isAuth: false
      }
    default:
      return state;
  }
}
