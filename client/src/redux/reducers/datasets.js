import { FETCH_DATASETS } from '../types';

const initialState = {
  list: []
}

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_DATASETS.SUCCESS:
      return {
        ...state,
        list: payload.datasets
      }
    default:
      return state;
  }
}
