import { SIGN_UP, SIGN_IN, CHECK_TOKEN, SIGN_OUT } from '../types';

const initialState = {
  data: {}
}

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case SIGN_IN.SUCCESS:
      return {
        ...state,
        data: payload.user
      }
    case SIGN_UP.SUCCESS:
      return {
        ...state,
        data: payload.user
      }
    case CHECK_TOKEN.SUCCESS:
      return {
        ...state,
        data: payload.user
      }
    case SIGN_OUT:
      return {
        ...state,
        data: null
      }
    default:
      return state
  }
}
