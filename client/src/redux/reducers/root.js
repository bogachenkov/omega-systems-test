import { combineReducers } from 'redux';

import authReducer from './auth';
import userReducer from './user';
import tokenReducer from './token';
import uiReducer from './ui';
import markupsReducer from './markups';
import usersReducer from './users';
import datasetsReducer from './datasets';

const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  token: tokenReducer,
  ui: uiReducer,
  markups: markupsReducer,
  users: usersReducer,
  datasets: datasetsReducer
});

export default rootReducer;
