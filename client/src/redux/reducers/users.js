import { FETCH_USERS, BLOCK_USER } from '../types';

const initialState = {
  list: []
}

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case FETCH_USERS.SUCCESS:
      return {
        ...state,
        list: payload.users
      }
    case BLOCK_USER.SUCCESS:
      console.log(payload);
      const updatedList = state.list.map(user => {
        if (user._id === payload.user._id) return { ...payload.user };
        return user;
      })
      return {
        ...state,
        list: updatedList
      }
    default:
      return state
  }
}
