import { CHECK_TOKEN, SIGN_OUT } from '../types';

const initialState = {
  checking: true
}

export default function(state = initialState, { type }) {
  switch (type) {
    case CHECK_TOKEN.PENDING:
      return {
        ...state,
        checking: true
      }
    case CHECK_TOKEN.FAILS:
      return {
        ...state,
        checking: false
      }
    case CHECK_TOKEN.SUCCESS:
      return {
        ...state,
        checking: false
      }
    case SIGN_OUT:
      return {
        ...state,
        checking: false
      }
    default:
      return state;
  }
}
