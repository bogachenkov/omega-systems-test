import { UI_PENDING_HANDLER, UI_SUCCESS_HANDLER, UI_FAILS_HANDLER, REMOVE_ERROR } from '../types';

const initialState = {
  requests: 0,
  error: '',
  toggle: false
}

export default function(state = initialState, {type, payload}) {
  switch (type) {
    case UI_PENDING_HANDLER:
      return {
        ...state,
        requests: state.requests + 1
      }
    case UI_SUCCESS_HANDLER:
      return {
        ...state,
        requests: state.requests - 1
      }
    case UI_FAILS_HANDLER:
      return {
        ...state,
        requests: state.requests - 1,
        toggle: !state.toggle,
        error: payload.error.message
      }
    case REMOVE_ERROR:
      return {
        ...state,
        error: ''
      }
    default:
      return state;
  }
};
