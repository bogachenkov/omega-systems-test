import React, { Component } from 'react';

import Container from '../../components/Container';
import JcropElement from '../../components/JcropElement';
import Button from '../../components/Button';
import AllViewed from '../../components/AllViewed';
import TwoToneIcon from '../../components/TwoToneIcon';

class MarkupsPage extends Component {

  state = {
    coords: []
  }

  onChange = coords => {
    this.setState({ coords });
  }

  nextImageHandler = () => {
    if (!this.checkCoords()) return;
    const { image, folder } = this.props;
    const { coords } = this.state;
    const data = {
      image,
      dataset: folder,
      coords
    }
    this.props.saveMarkup(data);
    this.setState({ coords: [] });
  }

  checkCoords = () => {
    const { coords } = this.state;
    return coords.length > 0;
  }

  render() {
    const { image, folder, allDone, requests } = this.props;
    const { coords } = this.state;
    return (
      <Container>
        <div className="heading">
          <h1 className="title">
            <TwoToneIcon size={24} icon="crop" />
            Разметка данных
          </h1>
          <Button
            onClick={this.nextImageHandler}
            disabled={!this.checkCoords()} >
            Следующее изображение
          </Button>
        </div>
        <section className="section">
            <div className="markups">
              {
                (allDone && requests < 1) &&
                <React.Fragment>
                  <AllViewed />
                  <p className="markups--message">Все изображения просмотрены</p>
                </React.Fragment>
              }
              {
                (!allDone && requests < 1) &&
                <React.Fragment>
                  <div className="markups--container">
                    <JcropElement
                      folder={folder}
                      image={image}
                      onChange={this.onChange}
                      coords={ coords }
                    />
                  </div>
                  <div className="markups--controls">

                  </div>
                </React.Fragment>
              }
            </div>
        </section>
      </Container>
    );
  }
}

export default MarkupsPage;
