import React, { Component } from 'react';
import {connect} from 'react-redux';
import MarkupsPage from './MarkupsPage.view';
import { fetchMarkups, saveMarkup } from '../../redux/actions';

import './markups.scss';

class MarkupsPageContainer extends Component {

  componentDidMount(props) {
    const { folder } = this.props.match.params;
    document.title="Разметка данных";
    this.props.fetchMarkups(folder);
  }

  render() {
    const { markups, saveMarkup, requests } = this.props;
    return (
        <MarkupsPage
          folder={this.props.match.params.folder}
          saveMarkup={saveMarkup}
          requests={requests}
          image={markups[0]}
          allDone={markups.length < 1}
        />
    );
  }
}

const mapStateToProps = state => ({
  requests: state.ui.requests,
  markups: state.markups.arr
});

const mapDispatchToProps = dispatch => ({
  fetchMarkups: folder => dispatch(fetchMarkups(folder)),
  saveMarkup: data => dispatch(saveMarkup(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(MarkupsPageContainer)
