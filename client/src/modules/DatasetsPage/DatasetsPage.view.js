import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Container from '../../components/Container';
import TwoToneIcon from '../../components/TwoToneIcon';
import DatasetItem from '../../components/DatasetItem';
import EmptyData from '../../components/EmptyData';

import './datasets-select.scss';

class DatasetsPage extends Component {
  render() {
    const { datasets } = this.props;
    return (
      <Container>
        <div className="heading">
          <h1 className="title">
            <TwoToneIcon size={24} icon="collections" />
            Выберите датасет
          </h1>
        </div>
        <div className="datasets-select">
          {
            datasets.length > 0 && datasets.map(({_id, title, folder}) =>
              <Link key={_id} to={`/datasets/${folder}`}>
                <DatasetItem title={title} folder={folder} />
              </Link>
            )
          }
        </div>
        {
          datasets.length === 0 && <EmptyData />
        }
      </Container>
    );
  }
}

export default DatasetsPage;
