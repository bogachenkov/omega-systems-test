import React, { Component } from 'react';
import {connect} from 'react-redux';
import DatasetsPage from './DatasetsPage.view';
import { fetchDatasets } from '../../redux/actions';

class DatasetsPageContainer extends Component {

  componentDidMount(props) {
    document.title = 'Выберите датасет';
    this.props.fetchDatasets();
  }

  render() {
    return (
      <DatasetsPage datasets={this.props.datasets} />
    );
  }
}

const mapStateToProps = state => ({
  datasets: state.datasets.list
});

const mapDispatchToProps = dispatch => ({
  fetchDatasets: () => dispatch(fetchDatasets())
});

export default connect(mapStateToProps, mapDispatchToProps)(DatasetsPageContainer);
