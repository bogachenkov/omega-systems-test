import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import Field from '../../components/Field';
import Button from '../../components/Button';

import './sign-in.scss';
import bgImg from './bg.jpg';

class SignIn extends Component {

  state = {
    name: '',
    password: ''
  }

  componentDidMount(props) {
    document.title = 'Вход'
  }

  changeNameHandler = e => {
    this.setState({
      name: e.target.value
    });
  }

  changePasswordHandler = e => {
    this.setState({
      password: e.target.value
    });
  }

  submitHandler = e => {
    e.preventDefault();
    if (!this.validateData()) return;
    const {signIn} = this.props;
    const {name, password} = this.state;
    const credentials = {
      name,
      password
    };
    signIn(credentials);
  }

  validateData = () => {
    const {name, password} = this.state;
    if (name.trim().length > 2 && password.trim().length > 5) return true;
    return false;
  }

  render() {
    const {name, password} = this.state;
    const {isAuth} = this.props;
    return (
      isAuth ? <Redirect to="/" /> :
      <div className="sign-in__bg" style={{backgroundImage: `url("${bgImg}")`}}>
        <div className="sign-in">
          <form onSubmit={this.submitHandler} className="sign-in__form"  autoComplete="banana">
            <h1 className="sign-in__title">Авторизация</h1>
            <Field
              value={name}
              onChange={this.changeNameHandler}
              placeholder="Имя"
            />
            <Field
              type="password"
              value={password}
              onChange={this.changePasswordHandler}
              placeholder="Пароль"
            />
            <Button
              disabled={!this.validateData()}
              isSubmit
            >
              Войти
            </Button>
          </form>
        </div>
      </div>
    );
  }
}

export default SignIn;
