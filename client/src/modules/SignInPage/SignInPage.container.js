import {connect} from 'react-redux';
import {signIn} from '../../redux/actions';

import SignInPage from './SignInPage.view';

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth,
  loading: state.auth.loading,
  error: state.auth.error
});

const mapDispatchToProps = dispatch => ({
  signIn: (credentials) => dispatch(signIn(credentials))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignInPage)
