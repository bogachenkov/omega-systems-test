import React from 'react';
import { Link } from 'react-router-dom';

import './not-found.scss';

const NotFoundPage = () => (
  <div className="not-found">
    <h1 className="not-found--h1">404</h1>
    <h3 className="not-found--h3">Страница не найдена</h3>
    <Link to="/">На главную</Link>
  </div>
);

export default NotFoundPage;
