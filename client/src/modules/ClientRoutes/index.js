import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import DatasetsPage from '../DatasetsPage';
import MarkupsPage from '../MarkupsPage';
import SignInPage from '../SignInPage';
import SignUpPage from '../SignUpPage';
import Navbar from '../Navbar';
import NotFoundPage from '../NotFoundPage';

import PrivateRoute from '../../hoc/PrivateRoute';

class ClientRoutes extends Component {
  render() {
    const { isAuth } = this.props;
    return (
      <React.Fragment>
        <Navbar />
        <Switch>
          <Route path="/signup" component={SignUpPage} />
          <Route path="/signin" component={SignInPage} />
          <PrivateRoute exact path="/datasets/" auth={isAuth} component={DatasetsPage} />
          <PrivateRoute exact path="/datasets/:folder" auth={isAuth} component={MarkupsPage} />
          <Route path="/" exact render={() => <Redirect to="/datasets" />} />
          <Route component={NotFoundPage} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default ClientRoutes;
