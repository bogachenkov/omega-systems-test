import {connect} from 'react-redux';
import SignUpPage from './SignUpPage.view';

import {signUp} from '../../redux/actions';

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth
});

const mapDispatchToProps = dispatch => ({
  signUp: credentials => dispatch(signUp(credentials))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpPage);
