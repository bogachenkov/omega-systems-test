import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

import Container from '../../components/Container';
import InlineField from '../../components/InlineField';
import Button from '../../components/Button';
import TwoToneIcon from '../../components/TwoToneIcon';

import './sign-up.scss';

class SignUp extends Component {

  state = {
    name: {
      value: '',
      label: 'Имя',
      helper: 'Имя пользователя. Минимум 3 символа',
      placeholder: 'Укажите Ваше имя',
      stateName: 'name'
    },
    password: {
      value: '',
      label: 'Пароль',
      helper: 'Пароль должен быть в длину как минимум 6 символов',
      placeholder: 'Введите пароль',
      type: 'password',
      stateName: 'password'
    },
    confirmPassword: {
      value: '',
      label: 'Повторите пароль',
      helper: 'Указанный выше пароль и этот должны совпадать',
      placeholder: 'Подтвердите пароль',
      type: 'password',
      stateName: 'confirmPassword'
    },
  }

  componentDidMount(props) {
    document.title = 'Регистрация';
  }

  handleFieldChange = (e, fieldName) => {
    this.setState({
      [fieldName]: {
        ...this.state[fieldName],
        value: e.target.value
      }
    });
  }

  checkFieldsComplete = () => {
    const {name, password, confirmPassword} = this.state;
    if (
      name.value.length > 2 &&
      password.value.length > 5 &&
      confirmPassword.value.length > 5 &&
      password.value === confirmPassword.value
    ) return true;
    return false;
  }

  handleSubmit = e => {
    e.preventDefault();
    if (!this.checkFieldsComplete()) return;
    const {name, password} = this.state;
    const credentials = {
      name: name.value,
      password: password.value
    }
    this.props.signUp(credentials);
  }

  render() {
    const {name, password, confirmPassword} = this.state;
    const fieldArray = [name, password, confirmPassword];
    const { isAuth } = this.props;

    return (
      isAuth ? <Redirect to="/" /> :
        <Container>
          <div className="heading">
            <h1 className="title">
              <TwoToneIcon size={24} icon="account_circle" />
              Регистрация
            </h1>
          </div>
          <section className="section">
            <form onSubmit={this.handleSubmit} className="sign-up">
              {fieldArray.map(({stateName, value, label, helper, placeholder, type}) => (
                <InlineField
                  key={stateName}
                  onChange={e => this.handleFieldChange(e, stateName)}
                  value={value}
                  label={label}
                  helper={helper}
                  placeholder={placeholder}
                  type={type}
                />
              ))}
              <Button
                disabled={!this.checkFieldsComplete()}
                isSubmit
              >
                Готово!
              </Button>
            </form>
          </section>
        </Container>
    );
  }
}

export default SignUp;
