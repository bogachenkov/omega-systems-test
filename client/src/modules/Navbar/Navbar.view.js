import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './navbar.scss';

class Navbar extends Component {

  signOutHandler = () => {
    this.props.signOut();
  }

  render() {
    const { isAuth } = this.props;
    return (
      <header className="navbar">
        <Link className="navbar__logo" to="/">
          App name
        </Link>
        <div>
          {isAuth &&
            <div>
              <Link className="navbar--link" to="/">Датасеты</Link>
              <Link className="navbar--link" to="/dashboard">Админка</Link>
              <p onClick={this.signOutHandler} className="navbar--link navbar__user">Выход</p>
            </div>
          }
          {!isAuth &&
            <div className="navbar__auth">
              <Link className="navbar--link navbar__signin" to="/signin">Войти</Link>
              <Link className="navbar__signup" to="/signup">Регистрация</Link>
            </div>
          }
        </div>
      </header>
    );
  }
}

export default Navbar;
