import {connect} from 'react-redux';
import {signOut} from '../../redux/actions';
import Navbar from './Navbar.view';

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth,
});

const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut())
});

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
