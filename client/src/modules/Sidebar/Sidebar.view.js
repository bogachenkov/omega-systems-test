import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom';
import './sidebar.scss';
import Button from '../../components/Button';
import TwoToneIcon from '../../components/TwoToneIcon';

class Sidebar extends Component {
  render() {
    return (
      <aside className="sidebar">
        <header className="sidebar--header">
          <Link className="sidebar--logo" to="/">App name</Link>
        </header>
        <Link to="/dashboard/datasets/add">
          <Button>Добавить датасет</Button>
        </Link>
        <NavLink to="/dashboard/users" className="sidebar--navlink">
          <TwoToneIcon icon="account_circle" size={16} />
          Пользователи
        </NavLink>
        <NavLink to="/dashboard/datasets" className="sidebar--navlink">
          <TwoToneIcon icon="collections" size={16} />
          Датасеты
        </NavLink>
        <div className="sidebar--links">
          <Link to="/">На главную</Link>
          <span onClick={this.props.signOut} to="/">Выход</span>
        </div>
      </aside>
    );
  }
}

export default Sidebar;
