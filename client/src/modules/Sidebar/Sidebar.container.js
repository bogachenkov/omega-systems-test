import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom';
import {signOut} from '../../redux/actions';
import Sidebar from './Sidebar.view';


const mapDispatchToProps = dispatch => ({
  signOut: () => dispatch(signOut())
});

export default withRouter(connect(null, mapDispatchToProps)(Sidebar));
