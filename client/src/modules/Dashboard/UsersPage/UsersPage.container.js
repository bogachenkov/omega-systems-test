import React, { Component } from 'react';
import {connect} from 'react-redux';
import UsersPage from './UsersPage.view';
import { fetchUsers, toggleBlockUser } from '../../../redux/actions';

class UsersPageContainer extends Component {

  componentDidMount(props) {
    document.title = 'Пользователи';
    this.props.fetchUsers();
  }

  render() {
    return <UsersPage {...this.props} />
  }
}

const mapStateToProps = state => ({
  users: state.users.list
});

const mapDispatchToProps = dispatch => ({
  fetchUsers: () => dispatch(fetchUsers()),
  toggleBlockUser: (user_id) => dispatch(toggleBlockUser(user_id))
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersPageContainer);
