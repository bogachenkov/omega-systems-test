import React, { Component } from 'react';
import m from 'moment';
import 'moment/locale/ru';

import DashboardHeader from '../../../components/DashboardHeader';
import TwoToneIcon from '../../../components/TwoToneIcon';
import EmptyData from '../../../components/EmptyData';

import './users.scss';

class UsersPage extends Component {
  render() {
    const { users, toggleBlockUser } = this.props;
    return (
      <React.Fragment>
        <DashboardHeader
          icon="account_circle"
          title="Пользователи"
          link="/dashboard/users/add"
          linkText="Добавить"
        />
        <div className="users">
          {
            users.length > 0 &&
            <table className="users--table">
              <thead>
                <tr>
                  <th>Имя</th>
                  <th>Дата регистрации</th>
                  <th>Разметил(а) изображений</th>
                  <th width="40"></th>
                </tr>
              </thead>
              <tbody>
                {users.map(user =>
                  <tr key={ user._id } className={ user.blocked ? 'user-blocked' : '' }>
                    <td className="users--name">
                      <TwoToneIcon icon="face" size={18} /> { user.name }
                    </td>
                    <td>{ m(user.createdAt).format('DD MMM YYYY') }</td>
                    <td>{ user.imagesMarked || 0 }</td>
                    <td className="users--block">
                      <p onClick={ () => toggleBlockUser(user._id) }>
                        <TwoToneIcon icon="block" size={ 18 } title="Заблокировать" />
                      </p>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          }
          {
            users.length === 0 && <EmptyData />
          }
        </div>
      </React.Fragment>
    );
  }
}

export default UsersPage;
