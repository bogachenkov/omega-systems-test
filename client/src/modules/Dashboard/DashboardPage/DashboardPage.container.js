import {connect} from 'react-redux';

import DashboardPage from './DashboardPage.view';

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth
});

export default connect(mapStateToProps)(DashboardPage);
