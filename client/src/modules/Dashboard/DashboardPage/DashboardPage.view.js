import React, { Component } from 'react';
import './dashboard.scss';
import { Switch, Route, Redirect } from 'react-router-dom';
import Sidebar from '../../Sidebar';
import DatasetsRoutes from '../DatasetsRoutes';
import UsersRoutes from '../UsersRoutes';

class DashboardPage extends Component {
  render() {
    return (
      <div className="dashboard">
        <Sidebar />
        <section className="dashboard-content">
          <Switch>
            <Route path="/dashboard/datasets" component={DatasetsRoutes} />
            <Route path="/dashboard/users" component={UsersRoutes} />
            <Route path="/dashboard" exact render={() => <Redirect to="/dashboard/users" />} />
          </Switch>
        </section>
      </div>
    );
  }
}

export default DashboardPage;
