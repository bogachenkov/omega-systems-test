import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import UsersPage from '../UsersPage';
import UsersAddPage from '../UsersAddPage';

class UsersRoutes extends Component {
  render() {
    const basePath = '/dashboard/users'
    return (
      <Switch>
        <Route path={`${basePath}/add`} component={UsersAddPage} />
        <Route exact path={`${basePath}/`} component={UsersPage} />
      </Switch>
    );
  }
}

export default UsersRoutes;
