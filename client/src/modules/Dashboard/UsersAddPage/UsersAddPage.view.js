import React, { Component } from 'react';
import DashboardHeader from '../../../components/DashboardHeader';
import InlineField from '../../../components/InlineField';
import Button from '../../../components/Button';

import './users-add.scss';

class UsersAddPage extends Component {

  state = {
    name: {
      value: '',
      label: 'Имя',
      helper: 'Имя пользователя',
      placeholder: 'Укажите имя пользователя',
      stateName: 'name'
    },
    password: {
      value: '',
      label: 'Пароль',
      helper: 'Пароль должен быть в длину как минимум 6 символов',
      placeholder: 'Введите пароль',
      type: 'password',
      stateName: 'password'
    }
  }

  componentDidMount(props) {
    document.title = 'Добавить пользователя'
  }

  handleFieldChange = (e, fieldName) => {
    this.setState({
      [fieldName]: {
        ...this.state[fieldName],
        value: e.target.value
      }
    });
  }

  checkFieldsComplete = () => {
    const {name, password} = this.state;
    if (
      name.value.length > 2 &&
      password.value.length > 5
    ) return true;
    return false;
  }

  handleSubmit = e => {
    e.preventDefault();
    if (!this.checkFieldsComplete()) return;
    const {name, password} = this.state;
    const credentials = {
      name: name.value,
      password: password.value
    }
    this.props.addUser(credentials);
  }

  render() {
    const {name, password} = this.state;
    const fieldArray = [name, password];

    return (
      <React.Fragment>
        <DashboardHeader
          icon="add"
          title="Добавить пользователя"
          link="/dashboard/users/"
          linkText="Назад"
        />
        <form onSubmit={this.handleSubmit} className="users-add">
          {fieldArray.map(({stateName, value, label, helper, placeholder, type}) => (
            <InlineField
              key={stateName}
              onChange={e => this.handleFieldChange(e, stateName)}
              value={value}
              label={label}
              helper={helper}
              placeholder={placeholder}
              type={type}
            />
          ))}
          <Button
            disabled={!this.checkFieldsComplete()}
            isSubmit
          >
            Готово!
          </Button>
        </form>
      </React.Fragment>
    );
  }
}

export default UsersAddPage;
