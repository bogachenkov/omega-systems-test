import {connect} from 'react-redux';
import UsersAddPage from './UsersAddPage.view';
import { addUser } from '../../../redux/actions';

const mapDispatchToProps = dispatch => ({
  addUser: credentials => dispatch(addUser(credentials))
});

export default connect(null, mapDispatchToProps)(UsersAddPage);
