import React, { Component } from 'react';

import DashboardHeader from '../../../components/DashboardHeader';
import DatasetItem from '../../../components/DatasetItem';
import EmptyData from '../../../components/EmptyData';

import './datasets.scss';

class DatasetsPage extends Component {
  render() {
    const { datasets } = this.props;

    return (
      <React.Fragment>
        <DashboardHeader
          icon="collections"
          title="Датасеты"
          link="/dashboard/datasets/add"
          linkText="Добавить"
        />
        <div className="datasets">
          {
            datasets.length > 0 && datasets.map(({_id, title, folder}) =>
              <DatasetItem key={_id} title={title} folder={folder} />
            )
          }
        </div>
        {
          datasets.length === 0 && <EmptyData />
        }
      </React.Fragment>
    );
  }
}

export default DatasetsPage;
