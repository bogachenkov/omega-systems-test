import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import DatasetsPage from '../DatasetsPage';
import DatasetsAddPage from '../DatasetsAddPage';

class DatasetsRoutes extends Component {
  render() {
    const basePath = '/dashboard/datasets';
    return (
      <Switch>
        <Route path={`${basePath}/add`} component={DatasetsAddPage} />
        <Route exact path={`${basePath}/`} component={DatasetsPage} />
      </Switch>
    );
  }
}

export default DatasetsRoutes;
