import React, { Component } from 'react';
import DashboardHeader from '../../../components/DashboardHeader';
import InlineField from '../../../components/InlineField';
import Button from '../../../components/Button';

class DatasetsAddPage extends Component {
  state = {
    title: {
      value: '',
      label: 'Название датасета',
      helper: 'Например, Автомобили',
      placeholder: 'Укажите название датасета',
      stateName: 'title'
    },
    folder: {
      value: '',
      label: 'Папка',
      helper: 'Название папки с изображениями, без косой черты (/) ',
      placeholder: 'Введите название папки',
      stateName: 'folder'
    }
  }

  componentDidMount(props) {
    document.title = 'Добавить датасет'
  }

  handleFieldChange = (e, fieldName) => {
    this.setState({
      [fieldName]: {
        ...this.state[fieldName],
        value: e.target.value
      }
    });
  }

  checkFieldsComplete = () => {
    const {title, folder} = this.state;
    if (
      title.value.length > 0 &&
      folder.value.length > 0
    ) return true;
    return false;
  }

  handleSubmit = e => {
    e.preventDefault();
    if (!this.checkFieldsComplete()) return;
    const {title, folder} = this.state;
    const credentials = {
      title: title.value,
      folder: folder.value
    }
    this.props.addDataset(credentials);
  }

  render() {
    const {title, folder} = this.state;
    const fieldArray = [title, folder];

    return (
      <React.Fragment>
        <DashboardHeader
          icon="add"
          title="Добавить датасет"
          link="/dashboard/datasets/"
          linkText="Назад"
        />
        <form onSubmit={this.handleSubmit} className="users-add">
          {fieldArray.map(({stateName, value, label, helper, placeholder, type}) => (
            <InlineField
              key={stateName}
              onChange={e => this.handleFieldChange(e, stateName)}
              value={value}
              label={label}
              helper={helper}
              placeholder={placeholder}
              type={type}
            />
          ))}
          <Button
            disabled={!this.checkFieldsComplete()}
            isSubmit
          >
            Готово!
          </Button>
        </form>
      </React.Fragment>
    );
  }
}

export default DatasetsAddPage;
