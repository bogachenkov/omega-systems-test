import {connect} from 'react-redux';
import DatasetsAddPage from './DatasetsAddPage.view';
import { addDataset } from '../../../redux/actions';

const mapDispatchToProps = dispatch => ({
  addDataset: credentials => dispatch(addDataset(credentials))
});

export default connect(null, mapDispatchToProps)(DatasetsAddPage);
