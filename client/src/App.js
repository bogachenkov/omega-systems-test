import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
import { checkToken } from './redux/actions';

import './app.scss';

import AppLoader from './components/AppLoader';
import DashboardPage from './modules/Dashboard/DashboardPage';
import ClientRoutes from './modules/ClientRoutes';
import Notifications from './components/Notifications';
import PrivateRoute from './hoc/PrivateRoute';

class App extends Component {

  componentDidMount() {
    this.props.checkToken();
  }

  render() {
    const {checkingToken, requests, isAuth} = this.props;
      return (
        checkingToken ? <AppLoader /> :
        <React.Fragment>
          { !!requests && <AppLoader /> }
          <Switch>
            <PrivateRoute path="/dashboard" auth={isAuth} component={DashboardPage} />
            <Route path="/" render={() => <ClientRoutes isAuth={isAuth} />} />
          </Switch>
          <Notifications />
        </React.Fragment>
      );
  }
}

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth,
  checkingToken: state.token.checking,
  requests: state.ui.requests
});

const mapDispatchToProps = dispatch => ({
  checkToken: () => dispatch(checkToken())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
