import React, { Component } from 'react';

import './empty-data.scss';

class EmptyData extends Component {
  render() {
    return (
      <p className="empty-data">
        Ничего не найдено
      </p>
    );
  }
}

export default EmptyData;
