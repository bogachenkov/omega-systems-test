import React from 'react';
import './inline-field.scss';

const InlineField = ({type = 'text', label, placeholder, helper, onChange, value, error}) => (
  <div className="inline-field">
    <label className="inline-field__label">
      {label}
    </label>
    <div className="inline-field__input">
      <input
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        className={error ? `error` : ''}
      />
      <p className="inline-field__helper">{helper}</p>
    </div>
  </div>
);

export default InlineField;
