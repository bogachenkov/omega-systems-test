import React from 'react';
import './button.scss';

const Button = ({children, disabled, isSubmit, onClick}) => (
  <button
    onClick={onClick}
    disabled={disabled}
    className={`button${disabled ? ' disabled' : ''}`}
    type={isSubmit ? "submit" : null}
  >
    {children}
  </button>
);

export default Button;
