import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import TwoToneIcon from '../TwoToneIcon';
import Button from '../Button';

import './dashboard-header.scss';

class DashboardHeader extends Component {
  render() {
    const { icon, title, link, linkText } = this.props;
    return (
      <header className="dashboard-header">
        <h1 className="dashboard-header--title">
          <TwoToneIcon icon={icon} size={30} />
          {title}
        </h1>
        <Link to={link}>
          <Button>
            {linkText}
          </Button>
        </Link>
      </header>
    );
  }
}

export default DashboardHeader;
