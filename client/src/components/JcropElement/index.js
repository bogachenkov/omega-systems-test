import React, { Component } from 'react';
import Jcrop from 'jcrop';

import './jcrop-element.scss';

class JcropElement extends Component {

  componentDidMount(props) {
    this.stage = Jcrop.attach('crop_image', {
      shadeColor: 'rgba(0, 0, 0, .75)',
      multi: true,
      widgetConstructor: customWidget
    });

    this.stage.listen('contextmenu', this.rightClickHandler);
    this.stage.listen('crop.change', this.cropChangeHandler);
    this.stage.listen('crop.remove', this.cropRemoveHandler);

  }

  componentWillUnmount() {
    this.stage.el.removeEventListener('crop.change', this.cropChangeHandler);
    this.stage.el.removeEventListener('contextmenu', this.rightClickHandler);
    this.stage.el.removeEventListener('crop.remove', this.cropRemoveHandler);
    this.stage.destroy();
  }

  rightClickHandler = (widget, e) => {
    e.preventDefault();
  }

  cropRemoveHandler = (widget, e) => {
    setTimeout(this.updateCropsCoords, 0);
  }

  cropChangeHandler = (widget, e) => {
    if (widget.pos.w === 0 || widget.pos.h === 0) return this.stage.removeWidget(widget);
    return this.checkIfCropChanged(widget) ? this.updateCropsCoords() : null;
  }

  checkIfCropChanged = (widget) => {
    const stateData = this.props.coords;
    const stageData = [...this.stage.crops].map(crop => ({...crop.pos}));
    if (stateData.length !== stageData.length) return true;
    let currentPos = widget.pos;
    return !stateData.some(crop => (
      crop.h === currentPos.h &&
      crop.w === currentPos.w &&
      crop.x === currentPos.x &&
      crop.y === currentPos.y
    ));
  }

  updateCropsCoords = () => {
    const stageCoords = [...this.stage.crops].map(crop => ({...crop.pos}));
    this.props.onChange(stageCoords);
  }

  render() {
    const { image, folder } = this.props;
    return (
      <img src={`/datasets/${folder}/${image}`} alt="" id="crop_image" style={{zIndex: '0'}} />
    );
  }
}

class customWidget extends Jcrop.Widget {
  init() {
    super.init();
    this.el.style.backgroundColor = 'rgba(255, 255, 255, .15)'
  }
}

export default JcropElement;
