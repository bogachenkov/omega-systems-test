import React from 'react';
import { connect } from 'react-redux';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import 'react-notifications/lib/notifications.css';

class Notifications extends React.Component {

  componentDidUpdate(prevProps, prevState) {
    const { error } = this.props;
    if (error) this.createNotification(error);
  }

  isEmpty = obj => {
    for (let key in obj) {
      return false;
    }
    return true;
  }

  createNotification = (message) => {
    NotificationManager.error(message, 'Ошибка!', 5000, () => {
      this.props.removeError()
    });
  };

  render() {
    return <div style={{fontSize: '14px'}}>
      <NotificationContainer/>
    </div>
  }
}

const mapStateToProps = state => ({
  error: state.ui.error,
  toggle: state.ui.toggle,
});

const mapDispatchToProps = dispatch => ({
  removeError: type => dispatch({ type: 'REMOVE_ERROR' })
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
