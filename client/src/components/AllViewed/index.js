import React from 'react';

import icon from './image.svg';

const AllViewed = () => (
  <div style={{textAlign: 'center', marginTop: '4em'}}>
    <img src={icon} alt="" />
  </div>
);

export default AllViewed;
