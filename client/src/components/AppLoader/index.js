import React from 'react';
import './app-loader.scss';

const AppLoader = () => (
  <div className="app-loader">
    <div className="lds-ripple"><div></div><div></div></div>
  </div>
);

export default AppLoader;
