import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Field extends Component {
  render() {
    const {onChange, value, placeholder, type = "text"} = this.props;
    return (
      <div>
        <input
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
      </div>
    );
  }
}

Field.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string,
}

export default Field;
