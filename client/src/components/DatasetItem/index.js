import React, { Component } from 'react';
import './dataset-item.scss';

class DatasetItem extends Component {
  render() {
    const { title, folder } = this.props;
    return (
      <article className="dataset-item">
        <div className="dataset-item--body">
          <p className="dataset-item--title">{title}</p>
          <pre className="dataset-item--folder">{folder}</pre>
        </div>
      </article>
    );
  }
}

export default DatasetItem;
