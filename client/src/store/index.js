import { createStore, applyMiddleware } from 'redux';

import logger from 'redux-logger';
import thunk from 'redux-thunk';
import uiMiddleware from '../redux/middleware/ui';

import rootReducer from '../redux/reducers';

const store = createStore(rootReducer, applyMiddleware(thunk, uiMiddleware, logger));

export default store;
