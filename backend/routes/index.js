module.exports = (app) => {
  app.use('/auth', require('./auth'));
  app.use('/markups', require('./markups'));
  app.use('/users', require('./users'));
  app.use('/datasets', require('./datasets'));
}
