const express = require('express');
const router = express.Router();
const passport = require('passport');

const markupsController = require('../controllers/markups');

router.get('/:folder', passport.authenticate('jwt', {session: false}), markupsController.fetchAll);
router.post('/save', passport.authenticate('jwt', {session: false}), markupsController.saveMarkup);

module.exports = router;
