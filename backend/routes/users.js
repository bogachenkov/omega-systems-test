const passport = require('passport');
const express = require('express');
const router = express.Router();

const usersController = require('../controllers/users');

router.get('/', passport.authenticate('jwt', { session: false }), usersController.fetchUsers);
router.post('/add', passport.authenticate('jwt', { session: false }), usersController.addUser);
router.put('/:user_id/block', passport.authenticate('jwt', { session: false }), usersController.toggleUsersBlock);

module.exports = router;
