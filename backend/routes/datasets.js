const passport = require('passport');
const express = require('express');
const router = express.Router();

const datasetsController = require('../controllers/datasets');

router.get('/', passport.authenticate('jwt', { session: false }), datasetsController.fetchAll);
router.post('/add', passport.authenticate('jwt', { session: false }), datasetsController.addDataset);

module.exports = router;
