const passport = require('passport');
const express = require('express');
const router = express.Router();

const authController = require('../controllers/auth');

router.post('/login', authController.login);
router.post('/register', authController.register);
router.get('/user', passport.authenticate('jwt', { session: false }), authController.getUser);

module.exports = router;
