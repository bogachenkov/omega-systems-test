require('dotenv').config()

const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");
const cors = require('cors');
const passport = require('passport');

const app = express();

// BodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Passport
app.use(passport.initialize());
require('./passport')(passport);

// MongoDB
mongoose.Promise = Promise;
mongoose.set('debug', true);
mongoose.connect(process.env.MONGO_URI)

app.use(logger("dev"));

app.use(cors());

// Static images
app.use('/datasets', express.static(__dirname + '/datasets'))

// Routes
require('./routes')(app);

module.exports = app;
