const mongoose = require('mongoose');
const { Schema } = mongoose;

const datasetSchema = new Schema({
  title: String,
  folder: String
})

const Dataset = mongoose.model('Dataset', datasetSchema);

module.exports = Dataset;
