const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name:  {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true,
    select: false
  },
  blocked: {
    type: Boolean,
    default: false
  },
  imagesMarked: {
    type: Number,
    default: 0
  }
},
{
  timestamps: true
})

const User = mongoose.model(`User`, UserSchema);

module.exports = User;
