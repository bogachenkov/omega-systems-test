const mongoose = require('mongoose');
const { Schema } = mongoose;

const MarkupSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  image: String,
  dataset: String,
  coords: [Object]
})

const Markup = mongoose.model('Markup', MarkupSchema);

module.exports = Markup;
