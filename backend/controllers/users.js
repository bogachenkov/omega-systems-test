const bcrypt = require('bcryptjs');
const User = require('../models/user');

const fetchUsers = (req, res) => {
  User.find()
    .then(users => res.json(users))
    .catch(error => res.status(404).json({
      message: error
    }))
}

const addUser = (req, res) => {
  let { name, password } = req.body;

  if (!name || !password) return res.status(400).json({
    message: 'Заполните все поля'
  });

  // Check if user already exists
  User.findOne({name})
    .then(user => {
      if (user) return res.status(400).json({
        message: 'Пользователь уже существует'
      })
      // Generate password's hash
      bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err;
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) throw err;
          new User({
            name,
            password: hash
          })
          .save()
          .then(user => res.json({user}))
        });
      });
    })
    .catch(error => res.status(400).json({
      message: error
    }));
}

const toggleUsersBlock = (req, res) => {
  User.findById(req.params.user_id)
    .then(user => {
      if (!user) return res.status(404).json({
        message: 'Пользователь не найден'
      })
      user.blocked = !user.blocked;
      return user.save()
    })
    .then(user => res.json(user))
    .catch(error => res.json({
      message: error
    }))
}

module.exports = {
  fetchUsers,
  addUser,
  toggleUsersBlock
}
