const Dataset = require('../models/dataset');
const fs = require('fs');

const fetchAll = (req, res) => {
  Dataset.find()
    .then(datasets => res.json(datasets))
    .catch(error => res.status(404).json({
      message: error
    }))
}

const addDataset = (req, res) => {
  console.log('Body: ', req.body);
  Dataset.findOne({folder: req.body.folder})
    .then(dataset => {
      console.log('DATASET', dataset);
      if (dataset) return res.status(400).json({
        message: 'Датасет с указанной папкой уже существует'
      });
      try {
        fs.accessSync(`datasets/${req.body.folder}`);
        new Dataset({
          title: req.body.title,
          folder: req.body.folder
        })
        .save()
        .then(dataset => res.json({dataset}))
      } catch (err) {
        return res.status(401).json({
           message: 'Указанная папка не найдена'
        })
      }
    })
    .catch(error => res.json({
      message: error
    }))
}

module.exports = {
  fetchAll,
  addDataset
}
