require('dotenv').config()
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

// Create jwt
const jwtSign = (user, res) => {
  jwt.sign(
    {
      name:  user.name,
      id: user._id
    },
    process.env.SECRET_KEY,
    { expiresIn: '1h' },
    (error, token) => {
      if (error) return res.status(400).json({error});
      return res.json({
        user: {
          id: user._id,
          name: user.name
        },
        token: `Bearer ${token}`,
      })
    }
  );
}

// Login
const login = (req, res) => {
  let { name, password } = req.body;

  if (!name || !password) return res.status(400).json({
    message: 'Заполните все поля'
  });

  // Check if user exists
  User.findOne({name}).select('+password')
    .then(user => {
      if (!user) return res.status(404).json({
        message: 'Пользователь не найден'
      });
      bcrypt.compare(password, user.password)
        .then(isMatch => {
          if (isMatch) jwtSign(user, res);
          else return res.status(400).json({
            message: 'Неверное сочетание логин/пароль'
          });
        })
    })
    .catch(error => res.status(400).json({
      message: error
    }));
}

// Register
const register = (req, res) => {
  let { name, password } = req.body;

  if (!name || !password) return res.status(400).json({
    message: 'Заполните все поля'
  });

  // Check if user already exists
  User.findOne({name})
    .then(user => {
      if (user) res.status(400).json({
        message: 'Данный логин занят'
      });
      // Generate password's hash
      bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err;
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) throw err;
          new User({
            name,
            password: hash
          })
          .save()
          .then(user => jwtSign(user, res))
        });
      });
    })
    .catch(error => res.status(400).json({
      message: error
    }));
}

// Get current user by token
const getUser = (req, res) => {
  User.findById(req.user.id)
    .then(user => {
      if (!user) return res.status(404).json({
        message: 'Пользователь не найден'
      });
      res.json({
        id: user.id,
        name: user.name
      });
    })
    .catch(error => res.status(400).json({
      message: error
    }))
}

module.exports = {
  login,
  register,
  getUser
}
