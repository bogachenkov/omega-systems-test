const passport = require('passport');
const fs = require('fs');

const Markup = require('../models/markup');
const User = require('../models/user');

// Get array of images, user hasn't marked
const fetchAll = (req, res) => {
  //Get images folder from params
  const imagesFolder = req.params.folder;
  // Get array of filenames
  fs.readdir(`datasets/${imagesFolder}`, (err, files) => {
    if (err) return res.status(500).json({
      message: 'Ошибка сервера: ' + err
    });
    // Get all images marked by user
    Markup.find({user: req.user.id, dataset: imagesFolder})
      .then(markups => {
        if (!markups) return res.json(files);
        // Get unmarked images
        const markedImagesArray = markups.map(item => item.image);
        const unmarkedImagesArray = files.filter(file => !markedImagesArray.includes(file));
        return res.json(unmarkedImagesArray);
      })
      .catch(error => res.status(400).json({
        message: error
      }));
  })
}

// Save coordinates of marked image
const saveMarkup = (req, res) => {
  const markup = {
    user: req.user.id,
    dataset: req.body.dataset,
    image: req.body.image,
    coords: req.body.coords
  }
  new Markup(markup).save()
    .then(markup => {
      User.findById(req.user.id)
        .then(user => {
          user.imagesMarked++;
          user.save()
            .then(() => res.json({name: markup.image}))
        })
    })
    .catch(error => res.status(500).json({
      message: error
    }))
}

module.exports = {
  fetchAll,
  saveMarkup
}
